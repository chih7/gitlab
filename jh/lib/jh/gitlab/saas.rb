# frozen_string_literal: true

module JH
  module Gitlab
    module Saas
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :com_url
        def com_url
          'https://gitlab.cn'
        end

        override :staging_com_url
        def staging_com_url
          'https://staging.gitlab.cn'
        end

        override :subdomain_regex
        def subdomain_regex
          %r{\Ahttps://[a-z0-9]+\.gitlab\.cn\z}.freeze
        end

        override :dev_url
        def dev_url
          'https://dev-ops.gitlab.cn'
        end
      end
    end
  end
end
