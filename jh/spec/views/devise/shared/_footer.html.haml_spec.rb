# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'devise/shared/_footer.html.haml' do
  include ApplicationHelper

  before do
    render
  end

  context 'footer renders as JH edition' do
    it 'renders About JiHu GitLab' do
      expect(rendered).to have_content('关于极狐(GitLab)')
    end
  end
end
